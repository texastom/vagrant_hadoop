# Instructions

## Step 0 - Install Vagrant

## Step 1 - Clone this repository
Once cloned, navigate within this directory. Let's call it {PROJECT_DIRECTORY}

## Step 2 - Downlod Hadoop
Download [Hadoop v.2.6.0](https://archive.apache.org/dist/hadoop/core/hadoop-2.6.0/hadoop-2.6.0.tar.gz)
Place the hadoop-2.6.0.tar.gz file in the vendor_assets directory. If it doesn't exist, create a `vendor_assets` directory at the same
level of this file. E.g. {PROJECT\_DIRECTORY}/vendor\_assets

## Step 3 - Vagrant
In terminal, execute `vagrant up` within {PROJECT_DIRECTORY}

## Step 4 - Login
Login to the vagrant box by execute `vagrant ssh`
You are now in the vagrant machine

Hadoop is stored in /usr/local/hadoop
Project files are in /vagrant/ *Note that this is a mirror of the repository directory on your host machine!


## Step 5 - Twitter App credentials
Copy {PROJECT_DIRECTORY}/app/src/twitter.properties.template, paste, and rename as twitter.properties. DO NOT REMOVE THE TEMPLATE FILE
Paste your credentials into the twitter.properties file. Save.

## Step 6 - Turn on HADOOP/HDFS/ETC.
Run `hdfs namenode -format`
Run `start-dfs.sh`
Run `start-yarn.sh`

## STEP 7 - Compile and execute application
Navigate to `/vagrant/app/src`

### Compile WordCountReducer
`javac -cp $HADOOP_HOME/share/hadoop/common/hadoop-common-2.6.0.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core 2.6.0.jar WordCountReducer.java`

### Compile WordCountMapper
`javac -cp $HADOOP_HOME/share/hadoop/common/hadoop-common-2.6.0.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.6.0.jar WordCountMapper.java`

### Compile WordCountController
`javac -classpath /usr/local/hadoop/share/hadoop/common/hadoop-common-2.6.0.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.6.0.jar:/vagrant/app/twitter4j-4.0.4/lib/twitter4j-core-4.0.4.jar:/usr/local/hadoop/share/hadoop/tools/lib/commons-logging-1.1.3.jar:/usr/local/hadoop/share/hadoop/tools/lib/*:/usr/local/hadoop/share/hadoop/common/lib/*:bin -d bin  WordCountController.java`

### Jar the class files
`jar cvf tweetsearcher.jar -C bin .`

### Add Twitte4j Library to Hadoop Classpath (do this only once)
`export HADOOP_CLASSPATH=/vagrant/app/twitter4j-4.0.4/lib/twitter4j-core-4.0.4.jar`

### Run WordCountController
`hadoop jar tweetsearcher.jar tweetexplorer.WordCountController`

### After successfull execution:
`TweetsFileHadoop.txt` is created in HDFS (/home/vagrant/)

`results` directory is created in HDFS (/home/vagrant/)


# Hadoop Overview
Vagrant forwards several HADOOP ports to the host machine so you can view the hadoop status in your browser. 

[Hadoop Overview](http://localhost:4567)

[Cluster Overview](http://localhost:4568)
