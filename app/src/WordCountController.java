package tweetexplorer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Scanner;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/*Steps of this code:
 * 1) Search for the tweets using Twitter4J.
 * 2) Save the tweets found on a file "TweetsFile.txt" locally.
 * 3) Upload the previous file to Hadoop.
 * 4) Map and Reduce processes.
 * 5) The final output table is stored in Hadoop as "table.txt"
 * 
 * Note: When the program is done on your console, you will have to wait for the "table.txt" file, to actually appear
 * on Hadoop. It doesn't appear immediately!.
 * Please also check your own tokens for Twitter4J and your paths locally and on Hadoop.  
 * 
 * How to run it: From Eclipse, right click on the project -> Export -> Java -> Runnable JAR file.
 * Launch configuration (selected the project one). Export destination (select one on your pc).  
 * Select "Extract require libraries...". Next until the end of the process (ignore warnings).
 * 
 * Once the jar file is generated, copy it to you virtual machine. Start yarn and hadoop as we did for the first lab.
 * Use "yarn jar /home/cosc/Desktop/WordCountApp.jar" to run the app.
 * When the process is done, open you browser and look for your destination path on hadoop. TweetsFile.txt and table.txt
 * should be there.*/

public class WordCountController extends Configured implements Tool{

	private String userDirectory = System.getProperty( "user.home" );
	
    // Local path to save TweetsFile.txt
	private Path tweetFileLocationInPC = new Path(userDirectory + "/TweetsFile.txt"); 

    // Hadoop Filesystem path to save TweetsFile.txt
	private Path tweetFileLocationInHadoop = new Path(userDirectory + "/TweetsFileHadoop.txt"); 

    // Hadoop Filesystem path to store results
	private Path tableLocationInHadoop = new Path(userDirectory + "/results");

	public static void main(String[] args)  {
		try {
            int res = ToolRunner.run( new Configuration(), new WordCountController(), args );
            System.exit( res );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
	}

	/* Map and Reduce (uses WordCountMapper.java and WordCountReducer.java)*/
	@Override
	public int run(String[] args) throws Exception {

		TweetSearcher tweetSearcher = new TweetSearcher( new File ( "twitter.properties" ) );
		QueryResult queryResult = tweetSearcher.query( "trump" );

		saveQueryResultToFile( queryResult, tweetFileLocationInPC );
		copyFileToHDFS( tweetFileLocationInPC, tweetFileLocationInHadoop );
		
		int result = runTwitterJob();
        return result;
	}

    private int runTwitterJob() throws IOException, InterruptedException, ClassNotFoundException {
        Job job = Job.getInstance( this.getConf() );

        job.setOutputKeyClass( Text.class );
        job.setOutputValueClass( IntWritable.class );

        job.setMapperClass( WordCountMapper.class );
        job.setReducerClass( WordCountReducer.class );

        job.setInputFormatClass( TextInputFormat.class );
        job.setOutputFormatClass( TextOutputFormat.class );

        FileInputFormat.setInputPaths( job, tweetFileLocationInHadoop );
        FileOutputFormat.setOutputPath( job, tableLocationInHadoop );

        job.setJarByClass( WordCountController.class );

        job.submit();
        return 0;
    }

    /**
     * Saves a Twitter4j QueryResult object's contents into a location
     * on the disk.
     *
     */
	private void saveQueryResultToFile( QueryResult queryResult, Path location ) {
		String stringLocation = location.toString();

        try {
            PrintWriter writer = new PrintWriter(stringLocation, "UTF-8"); // Create a file with the tweets retrieved.

            List<Status> tweets = queryResult.getTweets();
            int tweetNumber = 1;
            for (Status tweet : tweets) {
                String tweetText = /*tweetNumber + " @" + tweet.getUser().getScreenName() + " - " +*/ tweet.getText();
                System.out.println(tweetText);
                writer.println(tweetText); // Writes the tweet to a file.
                tweetNumber++;
            }
            writer.close(); // Close TweetsFile.txt

            System.out.println("\n\nTweetsFile.txt CREATED IN " + stringLocation + ".\n\n");
        } catch( FileNotFoundException ex ) {
            ex.printStackTrace();
            System.out.println("File not found: " + stringLocation );
        }  catch( UnsupportedEncodingException ex ) {
            ex.printStackTrace();
            System.out.println("UnsupportedEncodingException: " + stringLocation );
        }
	}

    private void copyFileToHDFS( Path fromDirectory, Path toDirectory ) {
        try {
            Configuration conf = new Configuration();
            conf.addResource(new Path("/home/user/hadoop/conf/core-site.xml"));
            conf.addResource(new Path("/home/user/hadoop/conf/hdfs-site.xml"));

            FileSystem hdfs = FileSystem.get( conf );

            hdfs.copyFromLocalFile(fromDirectory, toDirectory);    
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Problem copying file to HDFS");
        }
        

    }
}