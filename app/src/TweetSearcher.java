/**
 * A class that uses the twitter4j library to query twitter.
 */
package tweetexplorer;

import java.io.File;

import java.util.Scanner;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TweetSearcher {
	private File applicationCredentials;
	private String[] configurationProperties;
	private ConfigurationBuilder configurationBuilder;
	private Twitter twitterInstance;

	/**
	 * Initializes the configuration required for the twitter4j implementation
	 *
	 * @param configurationProperties the file containing the Twitter app credentials
	 */
	public TweetSearcher( File applicationCredentials ) {
		this.applicationCredentials = applicationCredentials;
		initOauthProperties();
		initOauthConfigurationBuilder();
	}

	/**
	 * Queries twitter using the twitter4j library and returns the results
	 * as a QueryResult object.
	 *
	 * @param theQuery the query to be searched for in Twitter
	 * @return A QueryResult object
	 * @see twitter4j.QueryResult
	 */
	public QueryResult query( String theQuery ) {
		if( twitterInstance == null ) {
			TwitterFactory tf = new TwitterFactory( configurationBuilder.build() );
			this.twitterInstance = tf.getInstance();
		}

		QueryResult queryResult = null;

		try {
			Query query = new Query( theQuery ); // What to look for
			query.count(5); // Limit of Tweets to retrieve (Max 100 per query)
			//query.resultType(query.MIXED); // POPULAR, MIXED or RECENT

			queryResult = twitterInstance.search( query );
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to search tweets: " + te.getMessage());
			System.exit(-1);
		}
		return queryResult;
	}

	/**
	 * Initializes a ConfigurationBuilder object required for twitter4j
	 * oauth. The created object is set to this.configurationBuilder
	 *
	 */
	private void initOauthConfigurationBuilder() {
		if( this.configurationBuilder == null ) {
			this.configurationBuilder = new ConfigurationBuilder();
		}

		this.configurationBuilder.setDebugEnabled( true )
			.setOAuthConsumerKey( configurationProperties[0] ) // Consumer Key
			.setOAuthConsumerSecret( configurationProperties[1] ) // Consumer Secret
			.setOAuthAccessToken( configurationProperties[2] ) // Access Token
			.setOAuthAccessTokenSecret( configurationProperties[3] ); // Token Secret
	}

	/**
	 * Initializes a String array object required for twitter4j
	 * oauth. The created object is set to this.configurationProperties.
	 *
	 * The array object will hold 4 values:
	 * 1. Consumer key
	 * 2. Consumer secret
	 * 3. Access token
	 * 4. Token secret
	 */
	private void initOauthProperties() {
		this.configurationProperties = readConfigurationFile();
	}

	/**
	 * Reads the configuration file set within the constructor and returns
	 * a string array with the application's credentials.
	 *
	 * @return A string array with the application's credentials.
	 * @see initOauthProperties()
	 */
	private String[] readConfigurationFile() {
		String[] returnString = null;
		try {
			// Get Twitter application credentials
			Scanner scanner = new Scanner( this.applicationCredentials );
			String consumerKey = scanner.nextLine().split( ":" )[1].trim();
			String consumerSecret = scanner.nextLine().split( ":" )[1].trim();
			String oauthAccessToken = scanner.nextLine().split( ":" )[1].trim();
			String oauthAccessTokenSecret = scanner.nextLine().split( ":" )[1].trim();

			returnString = new String[] { consumerKey, consumerSecret, oauthAccessToken, oauthAccessTokenSecret };

		} catch ( Exception ex ) {
			ex.printStackTrace();
			System.out.println("Failed to get twitter app credentials from twitter.properties file: " + ex.getMessage());
			System.exit(-1);
		}
		return returnString;
	}

}
