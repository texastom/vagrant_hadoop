#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# INSTALLS

apt-get update
sudo apt-get -y upgrade

# Install Apache/php5
apt-get install -y apache2
apt-get install -y php5

# No-prompt for mysql-server installation
debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"

apt-get -y install mysql-server
apt-get install php5-mysql

# The folder we'll use for our hadoop website
PROJECTFOLDER='hadoopdata'
sudo mkdir "/var/www/html/${PROJECTFOLDER}"

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/html/${PROJECTFOLDER}"
    <Directory "/var/www/html/${PROJECTFOLDER}">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf
sudo a2enmod rewrite
service apache2 restart

# Create the hadoop database 
mysql -u root -proot -e "CREATE DATABASE hadoop_data;"

# Create a `query_results` table
mysql -u root -proot "hadoop_data" -e "CREATE TABLE query_results (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	firstname VARCHAR(30) NOT NULL);"

# Java Open JDK 7
apt-get install -y openjdk-7-jdk

# Hadoop 2.4.1
# TODO: MIGHT NEED TO ASSIGN OWNERSHIP TO VAGRANT
cd /usr/local

# Use wget to download hadoop IF it wasn't included in the download
# wget https://archive.apache.org/dist/hadoop/core/hadoop-2.6.0/hadoop-2.6.0.tar.gz

# Use this line if the download included hadoop in the vendor_assets diretory
cp /vagrant/vendor_assets/hadoop-2.6.0.tar.gz .

tar xzf hadoop-2.6.0.tar.gz
rm hadoop-2.6.0.tar.gz

# Give vagrant access to the hadoop executables
sudo chown -R vagrant:vagrant /usr/local/hadoop-2.6.0
chmod -R 0755 /usr/local/hadoop-2.6.0

mv hadoop-2.6.0 hadoop

# -----------------------------------------------------------------------------
# ENVIRONMENT SETUP

# Add JAVA_HOME to environmental variables
source /home/vagrant/.bashrc && [ -z "$JAVA_HOME" ] && echo "export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64" >> /home/vagrant/.bashrc

# Add HADOOP_HOME to environmental variables
source /home/vagrant/.bashrc && [ -z "$HADOOP_HOME" ] && echo "export HADOOP_HOME=/usr/local/hadoop" >> /home/vagrant/.bashrc

# Add HADOOP_MAPRED_HOME
source /home/vagrant/.bashrc && [ -z "$HADOOP_MAPRED_HOME" ] && echo "export HADOOP_MAPRED_HOME=/usr/local/hadoop" >> /home/vagrant/.bashrc

# Add HADOOP_HDFS_HOME
source /home/vagrant/.bashrc && [ -z "$HADOOP_HDFS_HOME" ] && echo "export HADOOP_HDFS_HOME=/usr/local/hadoop" >> /home/vagrant/.bashrc

# Add YARN_HOME
source /home/vagrant/.bashrc && [ -z "$HADOOP_YARN_HOME" ] && echo "export HADOOP_YARN_HOME=/usr/local/hadoop" >> /home/vagrant/.bashrc

# Add HADOOP_COMMON_LIB_NATIVE_DIR
source /home/vagrant/.bashrc && [ -z "$HADOOP_COMMON_LIB_NATIVE_DIR" ] && echo "export HADOOP_COMMON_LIB_NATIVE_DIR=/usr/local/hadoop/lib/native" >> /home/vagrant/.bashrc

# Add HADOOP_INSTALL
source /home/vagrant/.bashrc && [ -z "$HADOOP_INSTALL" ] && echo "export HADOOP_INSTALL=/usr/local/hadoop" >> /home/vagrant/.bashrc

# Add HADOOP_BIN
source /home/vagrant/.bashrc && [ -z "$HADOOP_BIN" ] && echo "export HADOOP_BIN=/usr/local/hadoop/bin" >> /home/vagrant/.bashrc

# Add HADOOP_SBIN
source /home/vagrant/.bashrc && [ -z "$HADOOP_SBIN" ] && echo "export HADOOP_SBIN=/usr/local/hadoop/sbin" >> /home/vagrant/.bashrc

# Add HADOOP_SBIN to $PATH
[ -d "/usr/local/hadoop/sbin" ] && echo "export PATH=$PATH:/usr/local/hadoop/sbin:/usr/local/hadoop/bin" >> /home/vagrant/.bashrc

source /home/vagrant/.bashrc

# -----------------------------------------------------------------------------
# COPY HADOOP CONFIGURATION FILES

yes | cp -rf /vagrant/hadoop_config/* /usr/local/hadoop/etc/hadoop

# Creates an empty-passphrase RSA key for HADOOP to use when communicating
# with namenodes. 
ssh-keygen -b 2048 -t rsa -f /home/vagrant/.ssh/id_rsa -q -N ""
sudo chown -R vagrant:vagrant /home/vagrant/.ssh
cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
